package bleble103.fft;

import jline.console.ConsoleReader;
import sun.audio.AudioData;

import javax.sound.sampled.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static java.lang.System.currentTimeMillis;

public class Main {
    public static void main(String[] args){
        try {
            ConsoleReader console = new ConsoleReader();
            long lastOutput = System.currentTimeMillis();
            List<Complex> vector;
            List<Complex> transform;
            console.setPrompt(">");
            console.clearScreen();
            console.println("Sample rate: ");
            float sampleRate = Float.parseFloat(console.readLine());
            console.println("Samples (2^n): ");
            int samples = Integer.parseInt(console.readLine());
            SoundSampler sampler = new SoundSampler(sampleRate, samples);
            Transformator transformator = new Transformator();
            sampler.start();
            double[] frequencies = new double[samples / 2];
            console.println("Result resolution: ");
            int resultResolution = Integer.parseInt(console.readLine());
            double[] results = new double[resultResolution];
            int[] quantResults = new int[resultResolution];
            double maxi = 2000*(20.0/resultResolution);
            int nominalLevel = 50;
            int overDriveLevel = 70;

            while (true) {
                vector = sampler.sample();
                transform = transformator.transform(vector);
                //double maxi = 0;
                for (int i = 0; i < results.length; i++) {
                    results[i] = 0;
                    quantResults[i] = 0;
                }
                for (int i = 0; i < samples / 2; i++) {
                    frequencies[i] = transform.get(i).getModule();
                }
                for (int i = 0; i < samples / 2; i++) {
                    results[resultResolution * i / (samples / 2)] += frequencies[i];
                }
//                for (int i = 0; i < resultResolution; i++) {
//                    if (results[i] > maxi) maxi = results[i];
//                }
                for (int i = 0; i < resultResolution; i++) {
                    quantResults[i] = Math.min((int) (nominalLevel * results[i] / maxi), overDriveLevel);
                }

                if (System.currentTimeMillis() - lastOutput > 10) {
                    //console.print("\033[H\033[2J");  //clear screen
                    console.clearScreen();
                    //console.print(maxi+"\n\n");
                    lastOutput = System.currentTimeMillis();
                    for (int i = 0; i < resultResolution; i++) {
                        console.print(String.format("%5d ", (int) ((samples / 2) * i * sampleRate / samples / resultResolution)));
                        for (int j = 0; j < quantResults[i]; j++) console.print("#");
                        console.print("\n");
                        //System.out.println((i+1)+": \t"+quantResults[i]);
                    }
                    console.print("\n\n\n");
                    console.flush();
                }

            }
        }
        catch(IOException e){
            e.printStackTrace();
        }

//        for(int i=0; i< 2048; i++){
//            vector.add(new Complex(Math.random()));
//        }

        //SourceDataLine speakers;


//        vector.add(new Complex(1));
//        vector.add(new Complex(0));
//        vector.add(new Complex(-1));
//        vector.add(new Complex(0));
//        vector.add(new Complex(1));
//        vector.add(new Complex(0));
//        vector.add(new Complex(-1));
//        vector.add(new Complex(0));
//
//        Transformator transformator = new Transformator();
//        System.out.println("computing...");
//        List<Complex> result = transformator.transform(vector);
//        System.out.println("done");
//
//        for(Complex complex : result){
//            System.out.println(complex);
//        }
    }
}
