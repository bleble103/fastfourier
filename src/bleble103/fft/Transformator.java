package bleble103.fft;

import java.util.LinkedList;
import java.util.List;

public class Transformator {

    private List<Complex> concat(List<Complex> vector1, List<Complex> vector2){
        LinkedList<Complex> result = new LinkedList<>();
        for(int i=0; i<vector1.size(); i++){
            result.add(vector1.get(i));
        }

        for(int i=0; i<vector2.size(); i++){
            result.add(vector2.get(i));
        }

        return result;
    }

    private List<Complex> add(List<Complex> vector1, List<Complex> vector2){
        List<Complex> result = new LinkedList<>();
        for(int i=0; i<vector1.size(); i++){
            result.add(vector1.get(i).add(vector2.get(i)));
        }
        return result;
    }
    private List<Complex> subs(List<Complex> vector1, List<Complex> vector2){
        List<Complex> result = new LinkedList<>();
        for(int i=0; i<vector1.size(); i++){
            result.add(vector1.get(i).add( vector2.get(i).minus() ));
        }
        return result;
    }

    private List<Complex> eK(List<Complex> vector){
        List<Complex> result = new LinkedList<>();
        int k = vector.size();
        for(int i=0; i<k; i++){
            result.add(Complex.E(k,i,i).mul( vector.get(i) ));
        }
        return result;
    }

    public List<Complex> transform(List<Complex> vector){
        List<Complex> result;

        if(vector.size() == 2){
            result = new LinkedList<>();
            result.add( vector.get(0).add( vector.get(1) ) );
            result.add( vector.get(0).add( vector.get(1).minus() ) );
        }
        else{
            LinkedList<Complex> left = new LinkedList<>();
            LinkedList<Complex> right = new LinkedList<>();
            for(int i=0; i<vector.size(); i++){
                //perfect shuffle
                if(i%2 == 0) left.add(vector.get(i));
                else right.add(vector.get(i));
            }

            List<Complex> leftResult = transform(left);
            List<Complex> rightResult = transform(right);


            List<Complex> ekRightResult = eK(transform(right));

            result = concat(add(leftResult, ekRightResult), subs(leftResult, ekRightResult));
        }

        return result;
    }
}
