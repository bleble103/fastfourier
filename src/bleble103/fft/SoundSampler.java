package bleble103.fft;

import javax.sound.sampled.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class SoundSampler {
    AudioFormat format;
    TargetDataLine mic;
    //SourceDataLine speakers;
    private float sampleRate;
    private int samples;
    private int bias = 2048;

    public SoundSampler(float sampleRate, int samples){
        this.sampleRate = sampleRate;
        this.samples = samples;
        this.format = new AudioFormat(sampleRate,8,1,true, true);

    }

    public void start(){
        try {
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            DataLine.Info speakerInfo = new DataLine.Info(SourceDataLine.class, format);

            mic = (TargetDataLine) AudioSystem.getLine(info);
            //speakers = (SourceDataLine) AudioSystem.getLine(speakerInfo);
            mic.open(format);
            mic.start();
        }
        catch(Exception e){

            System.out.println(e.getMessage());
            e.getStackTrace();
        }
    }

    public List<Complex> sample(){
        List<Complex> result = new LinkedList<>();
        try{
            //mic = AudioSystem.getTargetDataLine(format);
            //DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            //DataLine.Info speakerInfo = new DataLine.Info(SourceDataLine.class, format);

            //mic = (TargetDataLine) AudioSystem.getLine(info);
            //speakers = (SourceDataLine) AudioSystem.getLine(speakerInfo);
            //mic.open(format);
            //speakers.open(format);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            int bytesRead = 0;
            byte[] data = new byte[samples];
            byte[] garbage = new byte[4098];
            int numBytesRead;
            //System.out.println("while");
            //mic.start();
            //speakers.start();
            while(bytesRead < samples){
                //mic.flush();
                //mic.read(garbage, 0, garbage.length); //read garbage
                numBytesRead = mic.read(data,0, data.length);
                //System.out.println(numBytesRead);
                outputStream.write(data, 0, numBytesRead);
                bytesRead += numBytesRead;
                //speakers.write(data, 0, numBytesRead);
            }
            //System.out.println("after while");

            //mic.close();

            byte[] outputData = outputStream.toByteArray();
            //speakers.write(outputData,0,outputData.length);
            //speakers.close();
            int count = 0;
            for(byte b : outputData){
                result.add(new Complex(1.0*b));
                count++;
                if(count == samples) break;
            }
            //System.out.println(maxi);
            //System.out.println(outputStream.size());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            e.getStackTrace();

        }

        return result;

    }
}
