package bleble103.fft;

public class Complex {
    private final double phi;
    private final double r;

    public static Complex ksi(int k, int n){
        double phi = 2*n*Math.PI/k;
        return new Complex(1, -phi);
    }

    public static Complex E(int k, int i, int j){
        if(i != j) return new Complex(0,0);

        return ksi(2*k, 1*j);
    }

    public Complex(double real){
        this.r = Math.abs(real);
        if(real < 0) this.phi = Math.PI;
        else this.phi = 0;
    }

    public Complex(double r, double phi){
        this.r = r;
        this.phi = phi;
    }

    public Complex mul(Complex other){
        double resultPhi = this.phi + other.phi;
        double resultR = this.r * other.r;
        return new Complex(resultR, resultPhi);
    }

    public double getReal(){
        return this.r * Math.cos(this.phi);
    }

    public double getImaginary(){
        return this.r * Math.sin(this.phi);
    }

    public Complex add(Complex other){
        double realPart = this.getReal() + other.getReal();
        double imaginaryPart = this.getImaginary() + other.getImaginary();
        double resultR = Math.sqrt(realPart*realPart + imaginaryPart*imaginaryPart);
        double resultPhi = Math.PI / 2;
        if(Math.abs(realPart) > 1e-3) resultPhi = Math.atan(imaginaryPart/realPart);
        if(realPart < 0) resultPhi += Math.PI;
        return new Complex(resultR, resultPhi);
    }

    public double getModule(){
        return this.r;
    }

    public Complex minus(){
        return new Complex(this.r, Math.PI + this.phi);
    }

    @Override
    public String toString(){
        return String.format("Complex re: %5f, im: %5f", this.getReal(), this.getImaginary());
    }
}
